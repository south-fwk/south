<?php
namespace South\Exception;

class RouterException extends BaseException
{

    protected $code = 1300;

    public function __construct($message, int $code = 0, \Exception $previous = null)
    {
        parent::__construct($message, $this->code, $previous);
    }

}
