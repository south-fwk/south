<?php
namespace South\Exception;

class DontExistsApplicationException extends BaseException
{

    protected $code = 1400;

    public function __construct($message, int $code = 0, \Exception $previous = null)
    {
        parent::__construct($message, $this->code, $previous);
    }

}
