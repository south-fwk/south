<?php
namespace South\Exception;

class RenderViewException extends BaseException
{

    protected $code = 1600;

    public function __construct($message, int $code = 0, \Exception $previous = null)
    {
        parent::__construct($message, $this->code, $previous);
    }

}
