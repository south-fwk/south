<?php
namespace South\Exception;

class ConfigurationException extends BaseException
{

    protected $code = 1500;

    public function __construct($message, int $code = 0, \Exception $previous = null)
    {
        parent::__construct($message, $this->code, $previous);
    }

}
