<?php
namespace South\Exception;

class BaseException extends \Exception implements \JsonSerializable
{

    protected $exception = null;

    public function setException(\Exception $exception)
    {
        $this->exception = $exception;
        $this->message = $this->exception->getMessage();
        $this->line = $this->exception->getLine();
        $this->code = $this->exception->getCode();
        $this->file = $this->exception->getFile();
    }

    protected function __trace()
    {

        if ($this->exception != null) {
            $trace = $this->exception->getTraceAsString();
        } else {
            $trace = $this->getTraceAsString();
        }

        $trace = \South\Util\Tools::removeReferencesNameSpace($trace);
        $trace = \South\Util\Tools::removeReferencesPath($trace);

        $trace = explode("\n", $trace);

        return $trace;
    }

    public function jsonSerialize()
    {

        $message = $this->message;
        $line = $this->line;
        $code = $this->code;
        $file = \South\Util\Tools::removeReferencesPath($this->file);

        $trace = $this->__trace();

        if ($this->exception != null) {
            $previous = $this->exception->getPrevious();
        } else {
            $previous = $this->getPrevious();
        }

        return array(
            "message" => $message,
            "code" => $code,
            "line" => $line,
            "file" => $file,
            "trace" => $trace,
            "previous" => $previous,
        );
    }
}
