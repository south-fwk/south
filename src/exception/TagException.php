<?php
namespace South\Exception;

class TagException extends BaseException
{

    protected $code = 1800;

    public function __construct($message, int $code = 0, \Exception $previous = null)
    {
        parent::__construct($message, $this->code, $previous);
    }

}
