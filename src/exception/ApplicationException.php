<?php
namespace South\Exception;

class ApplicationException extends BaseException
{

    protected $code = 1700;

    public function __construct($message, int $code = 0, \Exception $previous = null)
    {
        parent::__construct($message, $this->code, $previous);
    }

}
