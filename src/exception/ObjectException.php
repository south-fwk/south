<?php
namespace South\Exception;

class ObjectException extends BaseException
{

    protected $code = 1100;
    const SOUTH_OBJECT_NO_KEY = 1100;
    const SOUTH_OBJECT_NO_ARRAY = 1101;

    public function __construct($message, int $code, \Exception $previous = null)
    {

        if ($code >= 1100 && $code < 1200) {
            $this->code = $code;
        }
        parent::__construct($message, $this->code, $previous);

    }
}
