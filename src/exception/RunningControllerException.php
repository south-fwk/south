<?php
namespace South\Exception;

class RunningControllerException extends BaseException
{

    protected $code = 1200;

    public function __construct($message, int $code = 0, \Exception $previous = null)
    {
        parent::__construct($message, $this->code, $previous);
    }

}
