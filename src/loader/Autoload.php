<?php
namespace South\Loader;

use \South\Configuration\Provider as ConfigurationProvider;

final class Autoload
{
    const EXT_PHP = 'php';

    const CORE = 0;
    const PROJECT = 1;
    const EXTERNAL = 2;

    private $projectPath;

    /**
     * @var \South\Loader\Autoload
     */
    private static $instance = null;

    /**
     *
     * @return \South\Loader\Autoload
     *
     */
    public static function getInstance(): Autoload
    {
        if (self::$instance == null) {
            self::$instance = new self();
        }
        return self::$instance;
    }

    /**
     * @param string $prefix __NAMESPACE__ project
     * @param string $path __DIR__ project
     * @return \South\Loader\Autoload
     */
    public function project(string $prefix, string $path): Autoload
    {
        $this->projectPath = $path;
        $this->register($prefix, $path, true);
        return $this;
    }

    public function lib(int $scope, string $prefix, string $path, callable $refactorPath = null): Autoload
    {
        $beginPath = ConfigurationProvider::getInstance()->getCorePath() . DS . 'libs' . DS;
        if ($scope == self::PROJECT) {
            $beginPath = $this->projectPath . DS . 'libs' . DS;
        } else if ($scope == self::EXTERNAL) {
            $beginPath = '';
        }

        $this->register($prefix, $beginPath . $path, false, $refactorPath);
        return $this;
    }

    private function getFileTwig(string $className, string $prefix): string
    {
        $beginPath = ConfigurationProvider::getInstance()->getCorePath() . DS . 'libs' . DS;
        $prefix = str_replace('\\', '', $prefix);
        $base_dir = $beginPath . $prefix;
        $len = strlen($prefix);
        if (strncmp($prefix, $className, $len) !== 0) {
            return '';
        }
        $relative_class = substr($className, $len);

        $file = $base_dir . str_replace('_', '/', $relative_class) . '.php';
        $file = str_replace('\\', DS, $file);

        return $file;
    }

    public function register(string $prefix, string $path, bool $lower = true, callable $refactorPath = null): Autoload
    {
        spl_autoload_register(function ($className) use ($prefix, $path, $lower, $refactorPath) {
            $file = '';
            if (strpos($prefix, 'Twig') !== false) {
                $file = $this->getFileTwig($className, $prefix);
            } else {
                if (strpos($className, $prefix) !== false) {
                    if ($lower) {
                        $className = $this->refactorPath($this->transformInFile($prefix, $className));
                    } else {
                        if ($refactorPath != null) {
                            $className = $refactorPath($prefix, $className, $this);
                        } else {
                            $className = $this->transformInFile($prefix, $className);
                        }
                    }

                    $file = $path . DS . $className;

                }
            }
            if (is_file($file)) {
                require_once $file;
            }
        });
        return $this;
    }

    public function transformInFile($prefix, $className): string
    {
        $path = str_replace($prefix, '', $className);
        return str_replace('\\', DS, $path) . '.' . self::EXT_PHP;
    }

    private function refactorPath(string $className): string
    {
        $parts = explode(DS, $className);
        $path = '';
        foreach ($parts as $key => $part) {
            if ($key < \sizeof($parts) - 1) {
                $path .= strtolower($part) . DS;
            } else {
                $path .= $part;
            }
        }
        return !empty($path) ? $path : $className;
    }
}
