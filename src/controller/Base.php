<?php
namespace South\Controller;

class Base
{

    protected $route;

    final public function initialize(\South\Http\Route $route)
    {
        $this->route = $route;
    }

}
