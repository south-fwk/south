<?php
namespace South\Util;

abstract class Tools
{

    const EXT_PHP = 'php';

    public static function pr($data)
    {
        print('<pre>');
        print_r($data);
        exit;
    }

    public static function normalizePath(string $path)
    {
        return str_replace('\\', DS, $path);
    }

    public static function removeReferencesPath(string $path)
    {
        $configuration = \South\Configuration\Provider::getInstance();
        $path = str_replace($configuration->getApplicationPath(), 'project', $path);
        $path = str_replace($configuration->getCorePath(), 'core', $path);
        $path = str_replace('.' . self::EXT_PHP, '', $path);
        return $path;
    }

    public static function removeReferencesNameSpace(string $str)
    {
        $str = str_replace('South\\Project\\', '', $str);
        $str = str_replace('South\\', '', $str);
        return $str;
    }
}
