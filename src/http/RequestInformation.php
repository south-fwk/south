<?php
namespace South\Http;

use South\Http;
use \South\Configuration;

class RequestInformation
{
    private $uri;
    private $method;
    private $get;
    private $data;
    private $port;
    private $protocol;
    private $host;

    /**
     * @var \South\Http\RequestInformation
     */
    private static $instance = null;

    /**
     *
     * @return \South\Http\RequestInformation
     *
     */
    public static function getInstance(): RequestInformation
    {
        if (self::$instance == null) {
            self::$instance = new self();
        }
        return self::$instance;
    }

    public function __construct()
    {
        $configuration = Configuration\Provider::getInstance();
        $this->uri = $configuration->getRelativeRoute();
        $this->port = 80;
        $this->get = [];
        $this->method = Method::GET;
        $this->data = [];
        $this->protocol = Protocol::HTTP;
    }

    public function setHost(string $host): RequestInformation
    {
        $this->host = $host;
        return $this;
    }

    public function getHost(): string
    {
        return $this->host;
    }

    public function setUri(string $uri): RequestInformation
    {
        $this->uri = $uri;
        return $this;
    }

    public function getUri(): string
    {
        return $this->uri;
    }

    public function setMethod(string $method): RequestInformation
    {
        $this->method = strtoupper($method);
        return $this;
    }

    public function getMethod(): string
    {
        return $this->method;
    }

    public function setDataGet(array $get): RequestInformation
    {
        $this->get = new Parameters($get);
        return $this;
    }

    public function getDataGet(): Parameters
    {
        return $this->get;
    }

    public function setData(array $data): RequestInformation
    {
        $this->data = $data;
        return $this;
    }

    public function getData(): array
    {
        return $this->data;
    }

    public function setPort(int $port): RequestInformation
    {
        $this->port = $port;
        return $this;
    }

    public function getPort(): int
    {
        return $this->port;
    }

    public function setProtocol(string $protocol): RequestInformation
    {
        $this->protocol = strtolower($protocol);
        return $this;
    }

    public function getProtocol(): string
    {
        return $this->protocol;
    }

}
