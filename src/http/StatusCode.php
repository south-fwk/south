<?php
namespace South\Http;

abstract class StatusCode
{
    const OK = 200;
    const NOT_FOUND = 404;
    const UNAUTHORIZED = 401;
    const BAD_REQUEST = 400;
    const METHOD_NOT_ALLOWED = 405;
    const INTERNAL_ERROR_SERVER = 500;
    const BAD_GATEWAY = 502;
    const SERVICE_UNAVAILABLE = 503;
    const GATEWAY_TIMEOUT = 504;

    public static function getMessage(int $code): string
    {
        $messgs = [
            self::OK => 'OK',
            self::NOT_FOUND => 'Not Found',
            self::BAD_REQUEST => 'Bad Request',
            self::METHOD_NOT_ALLOWED => 'Method Not Allowed',
            self::INTERNAL_ERROR_SERVER => 'Internal Server Error',
            self::BAD_GATEWAY => 'Bad Gateway',
            self::SERVICE_UNAVAILABLE => 'Service Unavailable',
            self::GATEWAY_TIMEOUT => 'Gateway Timeout',
            self::UNAUTHORIZED => 'Unauthorized',
        ];
        return $messgs[$code];
    }

    public static function headerStatusCode(int $statusCode)
    {
        $phpSapiName = substr(php_sapi_name(), 0, 3);
        $mssg = StatusCode::getMessage($statusCode);
        if ($phpSapiName == 'cgi' || $phpSapiName == 'fpm') {
            header('Status: ' . $statusCode . ' ' . $mssg);
        } else {
            $protocol = isset($_SERVER['SERVER_PROTOCOL']) ? $_SERVER['SERVER_PROTOCOL'] : 'HTTP/1.0';
            header($protocol . ' ' . $statusCode . ' ' . $mssg);
        }
    }
}
