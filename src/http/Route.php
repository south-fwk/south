<?php
namespace South\Http;

use \South\Configuration\Provider as ConfigurationProvider;

class Route
{

    private $uri;
    private $path;
    private $redirect;
    private $method;
    private $controller;
    private $action;

    private $rules;
    private $rex;

    private $parameters;

    public function __construct(string $method = Method::GET, string $path = '', string $controller = '', array $rules = [])
    {

        $this->uri = null;
        $this->redirect = null;
        $this->setPath($method, $path);
        $this->setController($controller);
        $this->setRules($rules);
        $this->rex = null;
        $this->parameters = [];
    }

    public function setRules(array $rules): Route
    {
        $this->rules = $rules;
        return $this;
    }

    public function setPath(string $method, string $path): Route
    {
        $this->setMethod($method);
        $this->path = $path;
        $configurations = ConfigurationProvider::getInstance();

        try {
            $property = $configurations->get('router.redirect.' . $this->method . ".routes.$path");
            $path = $property->getValue();
            $this->redirect = new RouteRedirect($path);
        } catch (\Exception $e) {
            $this->redirectPath = $path;
        }

        return $this;
    }

    public function setMethod(string $method): Route
    {
        $this->method = $method;
        return $this;
    }

    public function setController(string $controller): Route
    {
        $controller = explode('::', $controller);
        $this->controller = $controller[0];
        if (isset($controller[1])) {
            $this->setAction($controller[1]);
        }
        return $this;
    }

    private function setParameters(array $parameters)
    {
        array_shift($parameters);

        $path = $this->path;

        preg_match_all('/({[a-z]+})+/i', $path, $output);

        array_shift($output);

        $parametersAux = [];

        if (\sizeof($output) > 0) {
            foreach ($parameters as $key => $parameter) {
                $paramKey = str_replace('{', '', str_replace('}', '', $output[0][$key]));
                $parametersAux = array_merge($parametersAux, array($paramKey => $parameter));
            }
        }

        $this->parameters = new Parameters($parametersAux);
    }

    public function matchTo(string $uri): bool
    {
        if ($this->rex == null) {
            $rex = $this->path;
            foreach ($this->rules as $key => $rule) {
                $rex = str_replace("{{$key}}", "($rule)", $rex);
            }

            $rex = str_replace('/', '\/', $rex);
            $rex = '/^' . $rex . '$/';
            $this->rex = $rex;
        }

        preg_match($this->rex, $uri, $output);
        $this->setParameters($output);

        $result = \sizeof($output) > 0;
        if ($result) {
            $this->uri = $uri;
            $this->refactorUriRedirect();
        }

        return $result;
    }

    private function refactorUriRedirect()
    {
        if ($this->redirect != null) {
            $uri = $this->redirect->getPath();
            foreach ($this->parameters as $key => $parameter) {
                $uri = str_replace("{{$key}}", $parameter, $uri);
            }
            $this->redirect->setUri($uri);
        }
    }

    public function setAction(string $action): Route
    {
        $this->action = $action;
        return $this;
    }

    public function getUri(): mixted
    {
        return $this->uri;
    }

    public function getPath(): string
    {
        return $this->path;
    }

    public function getRedirect(): mixed
    {
        return $this->redirect;
    }

    public function getMethod(): string
    {
        return $this->method;
    }

    public function getParameters(): Parameters
    {
        return $this->parameters;
    }

    public function getController(): string
    {
        return $this->controller;
    }

    public function getAction(): string
    {
        return $this->action;
    }

    public function __clone()
    {
        $this->uri = null;
        $this->parameters = null;
    }
}
