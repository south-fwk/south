<?php
namespace South\Http;

use \South\Configuration\Provider as ConfigurationProvider;

final class Router
{

    private $routes;
    private $lastRoute;
    private $currentRoute;
    private $uri;
    private $method;

    /**
     * @var \South\Http\Router
     */
    private static $instance = null;

    /**
     *
     * @return \South\Http\Router
     *
     */
    public static function getInstance(): Router
    {
        if (self::$instance == null) {
            self::$instance = new self();
        }
        return self::$instance;
    }

    public function __construct()
    {
        $this->routes = [];
        $this->lastRoute = null;
        $this->currentRoute = null;

        $request = Request::getInstance();
        $this->uri = $request->getRelativeUri();
        $this->method = $request->getInformation()->getMethod();
    }

    public function addRoute(Route $route): Router
    {

        if (!isset($this->routes[$route->getMethod()])) {
            $this->routes[$route->getMethod()] = [];
        }

        $this->lastRoute = $route;

        $this->routes[$route->getMethod()] = array_merge($this->routes[$route->getMethod()], array($route->getPath() => $route));
        if ($route->getMethod() == $this->method && $route->matchTo($this->uri)) {
            $this->currentRoute = $route;
        }

        return $this;
    }

    public function cloneAs(): Router
    {
        $methods = func_get_args();
        $route = $this->lastRoute;
        foreach ($methods as $method) {
            $routeClon = clone $route;
            $routeClon->setMethod($method);
            $this->addRoute($routeClon);
        }
        return $this;
    }

    public function load(string $path): Router
    {
        $path = str_replace('.', DS, $path);
        $file = ConfigurationProvider::getInstance()->getApplicationPath() . DS . 'routes' . DS . $path . '.php';
        include_once $file;

        return $this;
    }

    public function getRoutes(): array
    {
        return $this->routes;
    }

    public function getCurrentRoute(): Route
    {
        $route = $this->currentRoute;
        if ($route == null) {
            throw new \South\Exception\RouterException("Don't exists route");
        }
        return $this->currentRoute;
    }
}
