<?php
namespace South\Http\Api;

class Wrapper implements \JsonSerializable
{
    private $data;
    private $OK;
    private $request;
    private $serverInformation;
    private $error;
    protected $responseParameters;
    private $statusCode;

    public function __construct()
    {
        $this->data = null;
        $this->OK = true;
        $this->request = new Request();
        $this->serverInformation = new ServerInformation();
        $this->responseParameters = [];
        $this->error = null;
        $this->statusCode = \South\Http\StatusCode::OK;
    }

    protected function init(): Wrapper
    {

        if ($this->error != null) {
            $this->setOK(false);
            $this->setData(null);
        }
        $this->responseParameters["OK"] = $this->OK;
        $this->responseParameters["data"] = $this->data;
        $this->responseParameters["error"] = $this->error;
        $this->responseParameters["request"] = $this->request;
        $this->responseParameters["server"] = $this->serverInformation;

        return $this;
    }

    public function jsonSerialize()
    {
        $this->init();
        return $this->responseParameters;
    }

    public function setStatusCode(int $statusCode): Wrapper
    {
        $this->statusCode = $statusCode;
        return $this;
    }

    public function setData($data): Wrapper
    {
        $this->data = $data;
        return $this;
    }

    public function setOK(bool $ok): Wrapper
    {
        $this->OK = $ok;
        return $this;
    }

    public function getServerInformation(): ServerInformation
    {
        return $this->serverInformation;
    }

    public function setError(Error $error): Wrapper
    {
        $this->error = $error;
        return $this;
    }

    public function getError(): Error
    {
        return $this->error;
    }

    public function getStatusCode(): int
    {
        return $this->statusCode;
    }
}
