<?php
namespace South\Http\Api;

class Request implements \JsonSerializable
{
    private $method;
    private $url;

    public function __construct()
    {
        $request = \South\Http\Request::getInstance();
        $this->method = $request->getInformation()->getMethod();
        $this->url = $request->getUrl();
    }

    public function jsonSerialize()
    {
        return array(
            "method" => $this->method,
            "URL" => $this->url,
        );
    }
}
