<?php
namespace South\Http\Api;

class Error implements \JsonSerializable
{
    private $code;
    private $message;
    private $exception;
    public function __construct($code = null, string $message = '', $e = null)
    {
        $this->setCode($code);
        $this->setMessage($message);
        $this->setException($e);
    }

    public function jsonSerialize()
    {
        return array(
            "code" => $this->code,
            "message" => empty($this->message) ? null : $this->message,
            "exception" => $this->exception,
        );
    }

    public function setCode($code): Error
    {
        $this->code = (string) $code;
        return $this;
    }

    public function setMessage(string $message): Error
    {
        $this->message = $message;
        return $this;
    }

    public function setException($exception): Error
    {
        $this->exception = $exception;
        return $this;
    }

    public function getCode(): mixed
    {
        return $this->code;
    }

    public function getMessage(): string
    {
        return $this->message;
    }

    public function getException(): mixed
    {
        return $this->exception;
    }
}
