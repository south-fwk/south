<?php
namespace South\Http\Api;

class ServerInformation implements \JsonSerializable
{
    private $node;
    private $responseTime;
    private $excecutionDate;
    public function __construct()
    {
        $server = \South\Server\Server::getInstance();
        $this->node = \php_uname('n');
        $this->excecutionDate = date('c');

    }

    public function jsonSerialize()
    {
        return array(
            "node" => $this->node,
            "response_time" => $this->responseTime,
            "execution_date" => $this->excecutionDate,
        );
    }

    public function snapResponseTime(): ServerInformation
    {
        $server = \South\Server\Server::getInstance();
        $this->responseTime = microtime(true) - $server->REQUEST_TIME_FLOAT;
        return $this;
    }
}
