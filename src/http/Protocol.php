<?php
namespace South\Http;

abstract class Protocol
{
    const HTTP = 'HTTP';
    const HTTPS = 'HTTPS';
}
