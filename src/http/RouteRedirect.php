<?php
namespace South\Http;

class RouteRedirect
{
    private $uri;
    private $path;
    public function __construct(string $path = '', string $uri = '')
    {
        $this->setPath($path);
        $this->setUri($uri);
    }

    public function setPath(string $path)
    {
        $this->path = $path;
        return $this;
    }

    public function setUri(string $uri)
    {
        $this->uri = $uri;
        return $this;
    }

    public function getPath(): string
    {
        return $this->path;
    }

    public function getUri(): string
    {
        return $this->uri;
    }
}
