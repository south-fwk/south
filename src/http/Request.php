<?php
namespace South\Http;

use \South\Configuration\Provider as ConfigurationProvider;

final class Request
{

    private $url;
    private $information;

    /**
     * @var \South\Http\Request
     */
    private static $instance = null;

    /**
     *
     * @return \South\Http\Request
     *
     */
    public static function getInstance(): Request
    {
        if (self::$instance == null) {
            self::$instance = new self();
        }
        return self::$instance;
    }

    public function setInformation(RequestInformation $information): Request
    {
        $this->information = $information;
        return $this;
    }

    public function __construct()
    {

    }

    public function init()
    {
        $this->url = $this->information->getProtocol() . '://' . $this->information->getHost() . ($this->information->getPort() != 80 ? ':' . $$this->information->getPort() : '') . $this->information->getUri();
    }

    public function getRelativeUri(): string
    {

        $uri = $this->information->getUri();

        $relativeRoute = ConfigurationProvider::getInstance()->getRelativeRoute();
        $uri = '/' . str_replace($relativeRoute, '', $uri);
        if ($uri[strlen($uri) - 1] == '/') {
            $uri = substr($uri, 0, strlen($uri) - 1);
        }
        if (empty($uri)) {
            $uri = '/';
        }
        return $uri;
    }

    public function getUrl(): string
    {
        return $this->url;
    }

    public function getInformation(): RequestInformation
    {
        return $this->information;
    }

}
