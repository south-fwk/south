<?php
namespace South\View\Tags;

class Configurations extends Base
{

    public function __construct()
    {
        $this->setType('configurations');
    }

    public function get(string $html)
    {
        $htmlLocal = '';
        $matches = $this->getMatch($html);
        if ($matches && isset($matches[0])) {
            $match = $matches[0];
            $element = $this->getDomDocument($match);
            if ($this->isValid()) {
                $assets = $element->getElementsByTagName("assets");
                $assets = isset($assets[0]) ? $assets[0] : null;
                if ($assets) {
                    $this->setAssets($assets);
                }
            } else {
                $htmlLocal = $this->getError();
            }
            $html = str_replace($match, $htmlLocal, $html);
        }
        return $html;
    }

    private function setAssets(\DOMElement $assets)
    {
        $relativeRoute = \South\Configuration\Provider::getInstance()->getRelativeRoute();
        $oAssets = new \stdClass;

        if ($assets->getAttribute('path') !== null) {
            $oAssets->path = $relativeRoute . (string) $assets->getAttribute('path');
            $this->setJavaScript($oAssets, $assets);
            $this->setCss($oAssets, $assets);
            $this->setImages($oAssets, $assets);
            \South\Configuration\Provider::getInstance()->setConfigurationAssets($oAssets);
        }
    }

    private function setJavaScript(\stdClass &$oAssets, \DOMElement $assets)
    {
        $oAssets->js = new \stdClass;
        $js = $assets->getElementsByTagName("js");
        $js = isset($js[0]) ? $js[0] : null;
        if ($js) {
            if ($js->getAttribute('path') !== null) {
                $oAssets->js->path = $oAssets->path . (string) $js->getAttribute('path');
            }
        }
    }

    private function setCss(\stdClass &$oAssets, \DOMElement $assets)
    {
        $oAssets->css = new \stdClass;
        $css = $assets->getElementsByTagName("css");
        $css = isset($css[0]) ? $css[0] : null;
        if ($css) {
            if ($css->getAttribute('path') !== null) {
                $oAssets->css->path = $oAssets->path . (string) $css->getAttribute('path');
            }
        }
    }

    private function setImages(\stdClass &$oAssets, \DOMElement $assets)
    {
        $oAssets->images = new \stdClass;
        $images = $assets->getElementsByTagName("images");
        $images = isset($images[0]) ? $images[0] : null;
        if ($images) {
            if ($images->getAttribute('path') !== null) {
                $oAssets->images->path = $oAssets->path . (string) $images->getAttribute('path');
            }
        }
    }

}
