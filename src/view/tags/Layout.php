<?php
namespace South\View\Tags;

class Layout extends Base
{

    private $path;
    private $title;
    private $libraryVersion;

    public function __construct()
    {
        $this->setType('layout');
        $this->path = '';
        $this->libraryVersion = '1.0.0';
        $this->title = \South\Configuration\Provider::getInstance()->getApplicationName();
    }

    public function extractPath(string &$html)
    {

        $matches = $this->getMatch($html);
        if ($matches && isset($matches[0])) {
            $content = $this->getDomDocument($matches[0]);
            $html = str_replace($matches[0], '', $html);

            if ($content->getElementsByTagName('title')->length > 0) {
                $this->title = $content->getElementsByTagName('title')[0]->nodeValue;
            } else if (empty($this->title)) {

                try {
                    $route = \South\Http\Router::getInstance()->getCurrentRoute();
                    $this->title = $route->getController() . '.' . $route->getAction();
                } catch (\Exception $e) {
                    $this->title = ':::ERROR:::';
                }
            }

            if ($content->getElementsByTagName('library-version')->length > 0) {
                $this->libraryVersion = $content->getElementsByTagName('library-version')[0]->nodeValue;
            }

            if ($content->getAttribute('property-name') != null) {
                $name = \South\Configuration\Provider::getInstance()->get((string) $content->getAttribute('property-name'))->getValue();
            } else if ($content->getAttribute('name') != null) {
                $name = $content->getAttribute('name');
            } else {
                $name = '';
            }

            $appPath = \South\Configuration\Provider::getInstance()->getApplicationPath();
            $this->path = $appPath . DS . 'views' . DS . 'layouts' . DS . $name . '.phtml';
        }
        return $this;
    }

    public function getTitle()
    {
        return $this->title;
    }

    public function getPath()
    {
        return $this->path;
    }

    public function get(\South\View\parameters $parameters): string
    {
        $contents = '<s:contents />';
        if (!empty($this->path)) {
            if (!is_file($this->path)) {
                throw new \South\Exception\TagException("Don't exists file <strong>{$this->path}</strong>");
            }
            ob_start();
            $vp = $parameters;
            include $this->path;
            $contents = ob_get_contents();
            ob_end_clean();
            $o = ob_get_clean();
        }
        return $contents;
    }
}
