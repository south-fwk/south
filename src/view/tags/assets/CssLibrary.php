<?php
namespace South\View\Tags\Assets;

class CssLibrary extends Library
{
    private $template = ('<link rel="stylesheet"  href="{{resource}}" {{attrs}} />');

    public function get(): string
    {
        $html = str_replace("{{resource}}", $this->getPath(), $this->template) . "\n\t";
        $this->renderAttributes($html);
        return $html;
    }
}
