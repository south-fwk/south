<?php
namespace South\View\Tags\Assets;

class Collector implements \Iterator
{

    protected $storage = [];

    /**
     * @var \South\View\Tags\Assets\Collector
     */
    private static $instance = null;

    /**
     *
     * @return \South\View\Tags\Assets\Collector
     *
     */
    public static function getInstance(): Collector
    {
        if (self::$instance == null) {
            self::$instance = new self();
        }
        return self::$instance;
    }

    public function __construct()
    {}

    public function add(Library $library)
    {
        $b = false;
        foreach ($this->storage as $item) {
            if ($item->getPath() == $library->getPath()) {
                $b = true;
                break;
            }
        }
        if (!$b) {
            array_push($this->storage, $library);
        }
    }

    public function rewind()
    {
        return reset($this->storage);
    }

    public function current()
    {
        return current($this->storage);
    }

    public function key()
    {
        return key($this->storage);
    }

    public function next()
    {
        return next($this->storage);
    }

    public function valid()
    {
        return key($this->storage) !== null;
    }
}
