<?php
namespace South\View\Tags\Assets;

use South\View\Tags\Base;

class Assets extends Base
{

    private $match;
    public function __construct()
    {
        $this->setType('assets');
    }

    public function init(string $html): Assets
    {
        $matches = $this->getMatch($html);
        if ($matches && isset($matches[0])) {
            $this->match = $matches[0];
            $assets = $this->getDomDocument($this->match);
            if ($this->isValid()) {
                $this->registerLibraries($assets);
            }
        }
        return $this;
    }

    public function get(string $html)
    {
        $htmlLocal = '';
        if ($this->isValid()) {
            $this->addLibrariesToHtml($htmlLocal);
        } else {
            $htmlLocal = $this->getError();
        }
        $html = str_replace($this->match, $htmlLocal, $html);
        return $html;
    }

    private function addLibrariesToHtml(&$htmlLocal)
    {

        $collector = \South\View\Tags\Assets\Collector::getInstance();
        $htmJs = '';
        $htmCss = '';

        foreach ($collector as $k => $library) {
            if ($library instanceof JsLibrary) {
                $htmJs .= $library->get();
            } else if ($library instanceof CssLibrary) {
                $htmCss .= $library->get();
            }
        }

        $htmlLocal .= $htmCss;
        $htmlLocal .= $htmJs;

    }

    private function registerLibraries(\DOMElement $assets)
    {
        $list = $assets->getElementsByTagName("js");
        foreach ($list as $js) {
            \South\View\Tags\Assets\JavaScript::addToCollector($js);
        }

        $list = $assets->getElementsByTagName("css");
        foreach ($list as $js) {
            \South\View\Tags\Assets\Css::addToCollector($js);
        }
    }
};
