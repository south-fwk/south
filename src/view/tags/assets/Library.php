<?php
namespace South\View\Tags\Assets;

class Library
{

    protected $path;
    protected $attrs;
    protected $lookAttrs;

    public function __construct()
    {
        $this->path = '';
        $this->attrs = [];
        $this->lookAttrs = ['path', 'south', 'render', 'extern'];
    }

    public function setPath(string $path): Library
    {
        $this->path = $path;
        return $this;
    }

    public function getPath(): string
    {
        return $this->path;
    }

    public function get(): string
    {
        return '';
    }

    public function addAttribute(string $attr, string $value): Library
    {
        if (!in_array($attr, $this->lookAttrs)) {
            array_push($this->attrs, array($attr => $value));
        }
        return $this;
    }

    public function getAttributes(): array
    {
        return $this->attrs;
    }

    protected function renderAttributes(string &$html)
    {
        $attrs = [];
        foreach ($this->getAttributes() as $attr) {
            array_push($attrs, array_keys($attr)[0] . '="' . array_values($attr)[0] . '"');
        }
        $html = str_replace("{{attrs}}", implode(' ', $attrs), $html) . "\n\t";
    }
}
