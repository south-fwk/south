<?php
namespace South\View\Tags\Assets;

class JsLibrary extends Library
{
    private $template = ('<script type="text/javascript" src="{{resource}}" {{attrs}}></script>');

    public function get(): string
    {
        $html = str_replace("{{resource}}", $this->getPath(), $this->template) . "\n\t";
        $this->renderAttributes($html);
        return $html;
    }
}
