<?php
namespace South\View\Tags\Assets;

use South\View\Tags\Base;

class Css extends Base
{

    public function __construct()
    {
        $this->setType('css');
    }

    public static function addToCollector(\DOMElement $css)
    {
        $render = $css->getAttribute('render') == null ? true : filter_var($css->getAttribute('render'), FILTER_VALIDATE_BOOLEAN, FILTER_NULL_ON_FAILURE);
        $cssPath = \South\Configuration\Provider::getInstance()->getConfigurationAssets()->getCss()->getPath();
        $collector = \South\View\Tags\Assets\Collector::getInstance();
        if ($render) {

            if ($css->getAttribute('south') && filter_var($css->getAttribute('south'), FILTER_VALIDATE_BOOLEAN, FILTER_NULL_ON_FAILURE)) {
                $path = \South\Configuration\Provider::getInstance()->getRelativeRoute() . (string) $css->getAttribute('path') . '.css';
            } else {
                $path = $cssPath . (string) $css->getAttribute('path') . '.css';
            }

            $library = new CssLibrary();
            $library->setPath($path);

            if ($css->hasAttributes()) {
                foreach ($css->attributes as $attr) {
                    $library->addAttribute($attr->name, $attr->nodeValue);
                }
            }

            $collector->add($library);
        }
    }

    public function add(string &$html)
    {
        $matches = $this->getMatch($html);
        foreach ($matches as $k => $match) {
            $css = $this->getDomDocument($match);
            if ($this->isValid()) {
                self::addToCollector($css);
                $html = str_replace($match, '', $html);
            } else {
                $html = str_replace($match, $this->getError(), $html);
            }
        }
    }
}
