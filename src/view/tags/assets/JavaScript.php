<?php
namespace South\View\Tags\Assets;

use South\View\Tags\Base;

class JavaScript extends Base
{

    public function __construct()
    {
        $this->setType('js');
    }

    public static function addToCollector(\DOMElement $js)
    {
        $render = $js->getAttribute('render') == null ? true : filter_var($js->getAttribute('render'), FILTER_VALIDATE_BOOLEAN, FILTER_NULL_ON_FAILURE);
        $jsPath = \South\Configuration\Provider::getInstance()->getConfigurationAssets()->getJavaScript()->getPath();
        $collector = \South\View\Tags\Assets\Collector::getInstance();
        if ($render) {

            if ($js->getAttribute('south') && filter_var($js->getAttribute('south'), FILTER_VALIDATE_BOOLEAN, FILTER_NULL_ON_FAILURE)) {
                $path = \South\Configuration\Provider::getInstance()->getRelativeRoute() . (string) $js->getAttribute('path') . '.js';
            } else {
                $path = $jsPath . (string) $js->getAttribute('path') . '.js';
            }

            $library = new JsLibrary();
            $library->setPath($path);

            if ($js->hasAttributes()) {
                foreach ($js->attributes as $attr) {
                    $library->addAttribute($attr->name, $attr->nodeValue);
                }
            }

            $collector->add($library);
        }
    }

    public function add(string &$html)
    {
        $matches = $this->getMatch($html);
        foreach ($matches as $k => $match) {
            $js = $this->getDomDocument($match);
            if ($this->isValid()) {
                self::addToCollector($js);
                $html = str_replace($match, '', $html);
            } else {
                $html = str_replace($match, $this->getError(), $html);
            }
        }
    }
}
