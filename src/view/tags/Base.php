<?php
namespace South\View\Tags;

class Base
{

    const CORE = 'core';
    const LOCAL = 'local';
    const ABSOLUTE = 'absolute';

    private $type = '';
    private $error = '';
    private $htmlCode = '';

    protected function setType($type)
    {
        $this->type = $type;
    }

    protected function getHtmlFromNode($nodeO)
    {
        $html = '';
        $node = clone $nodeO;
        $children = $node->childNodes;
        foreach ($children as $child) {
            $tmp_doc = new \DOMDocument();
            $tmp_doc->appendChild($tmp_doc->importNode($child, true));
            $html .= $tmp_doc->saveHTML();
        }
        return $html;
    }

    protected function errorReset()
    {
        $this->error = '';
    }

    protected function getError()
    {
        return "
			<div style=\"margin:10px 0;border:1px solid #b00; padding:10px;\">
				<h3>[tag: {$this->type}] {$this->error}</h3>
				<div style=\"margin:5px 0; padding:10px; background:#eee;border:1px solid #ccc;\">
					" . str_replace("\t", "<span style=\"display:inline-block\">&nbsp;&nbsp;&nbsp;&nbsp;</span>", str_replace("\n", "<br />", htmlentities($this->htmlCode))) . "
				</div>
			</div>";
    }

    public function isValid()
    {
        if (!empty($this->error)) {
            return false;
        }
        return true;
    }

    protected function getAllMatch($match, $htmls)
    {
        preg_match_all($match, $htmls, $matches, PREG_PATTERN_ORDER);
        return $matches;
    }

    protected function getDomDocument($html)
    {
        $html = str_replace("&", "&amp;", $html);
        libxml_use_internal_errors(true);
        $dom = new \DOMDocument;
        $dom->strictErrorChecking = false;
        $dom->formatOutput = true;

        $this->htmlCode = $html;

        $dom->loadXML('<root xmlns:s="http://s/ns/1.0">' . $this->htmlCode . '</root>');

        $errors = libxml_get_errors();

        libxml_clear_errors();

        if (!empty($errors)) {
            $this->error = $errors[0]->message;
        }

        return $dom->getElementsByTagNameNS("http://s/ns/1.0", $this->type)->item(0);
    }

    protected function getMatch($out)
    {

        $matches = $this->getAllMatch("/<s:(" . $this->type . "){1}((\s+[^>]*)+\/\>|\/\>)/i", $out);
        $matches_ = array_map(function ($item) use (&$out) {
            $out = str_replace($item, "", $out);
            return $item;
        }, $matches[0]); #(\s+[.*]?|.*?)
        $matches2 = $this->getAllMatch("/<s:(" . $this->type . "){1}(.*)?>[\s\S]*?<\/s:(" . $this->type . ")>/i", $out);
        $matches_ = array_merge($matches_, array_map(function ($item) use (&$out) {
            $out = str_replace($item, "", $out);
            return $item;
        }, $matches2[0]));

        return $matches_;
    }
}
