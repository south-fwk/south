<?php
namespace South\View\Tags;

class Title extends Base
{

    public function __construct()
    {
        $this->setType('title');
    }

    private function getHtml(string $title)
    {
        return "<title>$title</title>";
    }

    public function get(string $html, string $title): string
    {
        $matches = $this->getMatch($html);
        if ($matches && isset($matches[0])) {
            $contents = $this->getDomDocument($matches[0]);
            $html = str_replace($matches[0], $this->getHtml($title), $html);
        }
        return $html;
    }
}
