<?php
namespace South\View\Tags;

use \South\View\Wrapper;

class Inc extends Base
{

    public function __construct()
    {
        $this->setType('inc');
    }

    public function get(string $html, \South\View\Parameters $parameters): string
    {
        $matches = $this->getMatch($html);
        foreach ($matches as $k => $match) {
            $element = $this->getDomDocument($match);
            $contents = '';
            if ($this->isValid()) {
                $basePath = \South\Configuration\Provider::getInstance()->getApplicationPath() . DS . 'views' . DS . 'includes' . DS;
                if ($element->getAttribute('scope') !== null) {
                    if ($element->getAttribute('scope') == self::CORE) {
                        $basePath = \South\Configuration\Provider::getInstance()->getCorePath() . DS . 'view' . DS . 'templates' . DS;
                    }
                }
                if ($element->getAttribute('render') === 'false') {
                    $contents = '';
                } else {
                    if ($element->getAttribute('path') !== '') {
                        $path = $element->getAttribute('path');
                        $path = str_replace('/', DS, $path);
                        $path = str_replace('.', DS, $path);
                        $file = $basePath . $path . '.phtml';

                        $contents = (new Wrapper($parameters))->getContents($file);

                        $inc = new Inc();
                        $contents = $inc->get($contents, $parameters);
                    }
                }
            } else {
                $contents = $this->getError();
                $this->errorReset();
            }
            $html = str_replace($match, $contents, $html);

        }
        return $html;
    }
}
