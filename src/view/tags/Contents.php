<?php
namespace South\View\Tags;

class Contents extends Base
{

    public function __construct()
    {
        $this->setType('contents');
    }

    public function get(string $html, string $view): string
    {
        $matches = $this->getMatch($html);
        if ($matches && isset($matches[0])) {
            $contents = $this->getDomDocument($matches[0]);
            if ($this->isValid()) {
                $html = str_replace($matches[0], $view, $html);
            } else {
                $html = str_replace($matches[0], $this->getError(), $html);
            }
        } else {

        }
        return $html;
    }
}
