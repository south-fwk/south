<?php
namespace South\View;

class View
{
    private $content;

    public function __construct(string $content)
    {
        $this->setContent($content);
    }

    public function setContent(string $content): View
    {
        $this->content = $content;
        return $this;
    }

    public function render()
    {
        header('Content-Type: text/html; charset=utf-8');
        print($this->content);
    }
}
