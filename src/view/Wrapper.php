<?php
namespace South\View;

class Wrapper extends \South\Object
{

    public function __construct(Parameters $parameters)
    {
        $keys = array_keys($parameters->get());
        foreach ($keys as $key) {
            $this->storage[$key] = $parameters->$key;
        }
    }

    public function getContents(string $path)
    {
        $contents = '';
        if (!is_file($path)) {
            throw new \South\Exception\TagException("Don't exists file <strong>{$path}</strong>");
        }
        ob_start();
        include $path;
        $contents = ob_get_contents();
        ob_end_clean();
        $o = ob_get_clean();
        return $contents;
    }
}
