<?php
namespace South\View;

class Html extends View
{
    const CORE = 'core';
    const LOCAL = 'local';
    const ABSOLUTE = 'absolute';

    private $scope;
    private $path;
    private $parameters;

    public function __construct(string $viewName = '', $scope = self::LOCAL)
    {
        $this->setParameters(new Parameters());
        $this->setPath($viewName, $scope);
    }

    public function setPath(string $viewName, $scope = self::LOCAL)
    {
        if ($scope == self::LOCAL) {
            $pathViews = \South\Configuration\Provider::getInstance()->getApplicationPath() . DS . 'views' . DS . 'actions' . DS;
        } else if ($scope == self::CORE) {
            $pathViews = \South\Configuration\Provider::getInstance()->getCorePath() . DS . 'view' . DS . 'templates' . DS;
        } else {
            $pathViews = '';
        }
        $this->path = $pathViews . "$viewName.phtml";
        return $this;
    }

    public function setParameters(Parameters $parameters)
    {
        $this->parameters = $parameters;
        return $this;
    }

    public function getParameters()
    {
        return $this->parameters;
    }

    public function get()
    {
        $contents = (new Wrapper($this->parameters))->getContents($this->path);
        $layout = new Tags\Layout();
        $html = $layout->extractPath($contents)->get($this->parameters);
        $confTag = new Tags\Configurations();
        $html = $confTag->get($html);

        if ($confTag->isValid()) {
            #$html = (new Tags\Inc())->get($html, $this->parameters);
            $contentsTag = new Tags\Contents();
            $html = $contentsTag->get($html, $contents);
            if ($contentsTag->isValid()) {
                $html = (new Tags\Title())->get($html, $layout->getTitle());

                $inc = new Tags\Inc();
                $html = $inc->get($html, $this->parameters);

                if ($inc->isValid()) {

                    $assets = new Tags\Assets\Assets();
                    $assets->init($html);

                    if ($assets->isValid()) {
                        $jsTag = new Tags\Assets\JavaScript();
                        $jsTag->add($html);

                        $jsTag = new Tags\Assets\Css();
                        $jsTag->add($html);
                    }

                    $html = $assets->get($html);
                }
            }
        }

        return $html;
    }
}
