<?php
namespace South;

final class Dispatcher
{

    /**
     * @var \South\Http\Dispatcher
     */
    private static $instance = null;

    /**
     *
     * @return \South\Http\Dispatcher
     *
     */
    public static function getInstance(): Dispatcher
    {

        if (self::$instance == null) {
            self::$instance = new self();
        }
        return self::$instance;
    }

    public function go()
    {
        $appType = '';
        try {

            $appType = Configuration\Provider::getInstance()->getApplicationType();
            App\Factory::analyzeSOUHTPetition();
            $app = App\Factory::get($appType);
            $app->run();

        } catch (Exception\RouterException $e) {

            if ($appType == App\Factory::API_REST) {
                App\Error\Api::exception($e, 'ROUTER_EXCEPTION');
            } else if ($appType == App\Factory::WEB_APP) {
                App\Error\Web::exception($e, 'ROUTER_EXCEPTION');
            } else {
                throw $e;
            }

        } catch (\Exception $e) {
            if ($appType == App\Factory::API_REST) {
                App\Error\Api::exception($e);
            } else if ($appType == App\Factory::WEB_APP) {
                App\Error\Web::exception($e);
            } else {
                throw $e;
            }

        }
    }

}
