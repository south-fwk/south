<?php
namespace South\Server;

use \South\Object;

final class Server extends Object
{
    public function __construct()
    {
        parent::__construct($_SERVER);
    }

    /**
     * @var \South\Server\Server
     */
    private static $instance = null;

    /**
     *
     * @return \South\Server\Server
     *
     */
    public static function getInstance(): Server
    {
        if (self::$instance == null) {
            self::$instance = new self();
        }
        return self::$instance;
    }
}
