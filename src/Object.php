<?php
namespace South;

use \South\Exception\ObjectException;

class Object implements \Iterator, \JsonSerializable
{

    protected $storage;

    public function __construct(array $storage = [])
    {
        $this->setStorage($storage);
    }

    public function setStorage(array $storage)
    {
        $this->storage = $storage;
        return $this;
    }

    public function get(): array
    {
        return $this->storage;
    }

    public function __set($key, $value)
    {

        if (array_key_exists($key, $this->storage)) {
            $this->storage[$key] = $value;
        }
        if (array_key_exists(str_replace('_', '-', $key), $this->storage)) {
            $this->storage[str_replace('_', '-', $key)] = $value;
        }

        $this->storage[$key] = $value;
    }

    public function __get($key)
    {
        if (array_key_exists($key, $this->storage)) {
            return $this->storage[$key];
        }
        if (array_key_exists(str_replace('_', '-', $key), $this->storage)) {
            return $this->storage[str_replace('_', '-', $key)];
        }
        throw new ObjectException("Don't exists property [$key]", ObjectException::SOUTH_OBJECT_NO_KEY);
    }

    public function __call($key, $args = array()): bool
    {

        if (\sizeof($args) == 1 && is_string($args[0])) {
            switch ($args[0]) {
                case 'r':
                    $this->__remove($key);
                    return true;
                case 'o' || 't':
                    if (!is_array($this->storage[$key])) {
                        throw new ObjectException("[$key] is not an array", ObjectException::SOUTH_OBJECT_NO_ARRAY);
                    }
                    $this->storage[$key] = $this->transform($key, $args[0]);

                    return true;
            }
        }

        if (array_key_exists($key, $this->storage)) {
            return true;
        }

        if (array_key_exists(str_replace('_', '-', $key), $this->storage)) {
            return true;
        }

        return false;
    }

    private function transform(string $key, string $type): self
    {
        if ($type == 'o') {
            $class = '\South\Object';
        } else {
            $class = get_called_class();
        }
        return new $class($this->storage[$key]);
    }

    private function __remove(string $key)
    {
        unset($this->storage[$key]);
    }

    public function rewind()
    {
        return reset($this->storage);
    }
    public function current()
    {
        return current($this->storage);
    }
    public function key()
    {
        return key($this->storage);
    }
    public function next()
    {
        return next($this->storage);
    }
    public function valid()
    {
        return key($this->storage) !== null;
    }

    public function jsonSerialize()
    {
        return $this->storage;
    }
}
