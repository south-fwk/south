<?php
namespace South\App;

use \South\Http\StatusCode;

class Web extends Application implements IApplication
{

    public function run()
    {

        Error\Web::register();

        if (!($this->controller instanceof \South\Controller\Web)) {
            throw new \South\Exception\ApplicationException("Application type dont't allow parent type for [{$this->controllerName}]");
        }

        try {
            $result = call_user_func_array(array($this->controller, $this->route->getAction()), $this->route->getParameters()->get());
        } catch (\Exception $e) {

            throw new \South\Exception\RunningControllerException("An error occurred while running controller [{$this->controllerName}]", 0, $e);
        }

        if (!($result instanceof \South\View\View) && !is_string($result)) {
            throw new \South\Exception\ApplicationException("Result type dont't allow for [{this->controllerName}]");
        }

        $this->headers(\South\Http\StatusCode::OK);

        try {
            if (is_string($result)) {
                print($result);
            } else {

                $result->render();
            }
        } catch (\South\Exception\TagException $e) {
            throw $e;
        } catch (\Exception $e) {
            throw new \South\Exception\RenderViewException("An error occurred while rendering view at controller [{$this->controllerName}]", 0, $e);
        }

        exit;
    }

    private function headers(int $statusCode)
    {
        \South\Http\StatusCode::headerStatusCode($statusCode);
    }
}
