<?php
namespace South\App\Resource;

class Provider
{

    /**
     * @var \South\App\Resource\Provider
     */
    private static $instance = null;

    /**
     *
     * @return \South\App\Resource\Provider
     *
     */
    public static function getInstance(): Provider
    {

        if (self::$instance == null) {
            self::$instance = new self();
        }
        return self::$instance;
    }

    public function sendResourse()
    {

        $parameters = \South\Http\Request::getInstance()->getInformation()->getDataGet();
        $type = $parameters->type;
        $resource = $parameters->resource;
        $path = \South\Configuration\Provider::getInstance()->getCorePath() . DS . 'assets' . DS . $type . DS . $resource;
        if (is_file($path) && is_readable($path)) {
            switch ($type) {
                case 'css':

                    header("Content-type: text/css; charset: UTF-8");
                    $contents = file_get_contents($path);
                    print($contents);

                    break;

                case 'js':

                    header("Content-type: application/javascript; charset: UTF-8");
                    $contents = file_get_contents($path);
                    print($contents);

                    break;

                default:
                    break;
            }
        }
        exit;
    }

}
