<?php
namespace South\App;

abstract class Factory
{
    const WEB_APP = 'web';
    const API_REST = 'api';

    public static function get(string $type): Application
    {
        if ($type == self::WEB_APP) {
            return new Web();
        }
        if ($type == self::API_REST) {
            return new Api();
        }
        throw new \South\Exception\DontExistsApplicationException("Don't exists application type");
    }

    public static function analyzeSOUHTPetition()
    {
        $parameters = \South\Http\Request::getInstance()->getInformation()->getDataGet();
        if ($parameters->SOUHT_ASSETS()) {
            Resource\Provider::getInstance()->sendResourse();
        }
    }
}
