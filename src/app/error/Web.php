<?php
namespace South\App\Error;

abstract class Web
{

    public static function register()
    {
        register_shutdown_function(function () {
            $error = error_get_last();
            if (count($error) > 0) {self::shutdown($error);}
        });
        set_exception_handler(function ($exception) {
            self::exceptionHandler($exception);
        });
        set_error_handler(function ($numero, $mensaje, $archivo, $linea) {
            self::errorHandler($numero, $mensaje, $archivo, $linea);
        });
    }

    private static function shutdown($error)
    {
        #\South\Util\Tools::pr($error);
        $exception = new \stdClass;
        $exception->scope = 'SHUTDOWN';
        $exception->message = self::removeAllReferences($error["message"]);
        $exception->code = null;
        $exception->type = $error['type'];
        $exception->line = $error["line"];
        $exception->file = \South\Util\Tools::removeReferencesPath($error["file"]);
        $exception->trace = self::debugStringBacktrace();
        $exception->document = self::highlightDocument($error["file"], $error["line"]);
        self::show($exception);
    }

    private static function exceptionHandler($_exception)
    {
        $exception = new \stdClass;
        $exception->scope = 'EXCEPTION_HANDLER';
        $exception->message = self::removeAllReferences($_exception->getMessage());
        $exception->code = $_exception->getCode();
        $exception->line = $_exception->getLine();
        $exception->file = \South\Util\Tools::removeReferencesPath($_exception->getFile());
        $exception->trace = explode(PHP_EOL, self::removeAllReferences($_exception->getTraceAsString()));
        $exception->document = self::highlightDocument($_exception->getFile(), $_exception->getLine());

        #\South\Util\Tools::pr($exception); exit;

        self::show($exception);
    }

    private static function errorHandler($code, $message, $file, $line)
    {
        $exception = new \stdClass;
        $exception->scope = 'ERROR_HANDLER';
        $exception->message = self::removeAllReferences($message);
        $exception->code = $code;
        $exception->line = $line;
        $exception->file = \South\Util\Tools::removeReferencesPath($file);
        $exception->trace = self::debugStringBacktrace();
        $exception->document = self::highlightDocument($file, $line);

        self::show($exception);
    }

    private static function headers(int $statusCode)
    {
        \South\Http\StatusCode::headerStatusCode($statusCode);
        header('Content-Type: application/json; charset=utf-8');
    }

    public static function exception($_exception, $code = '')
    {
        $exception = new \stdClass;
        $exception->scope = empty($code) ? 'EXCEPTION_UNCRONTROLLED' : $code;
        $exception->message = self::removeAllReferences($_exception->getMessage());
        $exception->code = self::getCodeForHighlightDocument($_exception);
        $exception->line = self::getLineForHighlightDocument($_exception);
        $exception->file = self::getFileForHighlightDocument($_exception);
        $exception->trace = array_merge(self::debugStringBacktrace(), self::getPreviousTrace($_exception->getPrevious()));
        $exception->document = self::highlightDocument($exception->file, $exception->line);
        $exception->file = \South\Util\Tools::removeReferencesPath($exception->file);

        self::show($exception);
    }

    private static function getPreviousTrace($previous)
    {
        $trace = [];
        if ($previous != null) {
            array_push($trace, self::removeAllReferences("
				<div class=\"s-previous\">
					<strong>PREVIOUS</strong>: {$previous->getMessage()}
					<br />
					<span class=\"s-previous-file\">{$previous->getFile()}</span> (<span class=\"s-previous-line\">{$previous->getLine()}</span>)
					]
				</div>
			"));
            $trace = array_merge($trace, explode(PHP_EOL, self::removeAllReferences($previous->getTraceAsString())));

            $trace = array_reverse($trace);
            array_shift($trace);
            $trace = array_reverse($trace);

            if ($previous->getPrevious() != null) {
                $trace = array_merge($trace, self::getPreviousTrace($previous->getPrevious()));
            }
        }
        return $trace;
    }

    private static function getFileForHighlightDocument(\Exception $exception): string
    {
        if ($exception->getPrevious() == null) {
            return $exception->getFile();
        }
        return self::getFileForHighlightDocument($exception->getPrevious());
    }

    private static function getLineForHighlightDocument(\Exception $exception): int
    {
        if ($exception->getPrevious() == null) {
            return $exception->getLine();
        }
        return self::getLineForHighlightDocument($exception->getPrevious());
    }

    private static function getCodeForHighlightDocument(\Exception $exception): int
    {
        if ($exception->getPrevious() == null) {
            return $exception->getCode();
        }
        return self::getCodeForHighlightDocument($exception->getPrevious());
    }

    private static function show(\stdClass $exception)
    {
        $vHtml = new \South\View\Html('errors/index', \South\View\Html::CORE);
        $parameters = $vHtml->getParameters();
        $parameters->exception = $exception;
        (new \South\View\View($vHtml->get()))->render();
        exit;
    }

    private static function removeAllReferences(string $string): string
    {
        return \South\Util\Tools::removeReferencesNameSpace(\South\Util\Tools::removeReferencesPath($string));
    }

    private static function debugStringBacktrace(): array
    {
        ob_start();
        debug_print_backtrace(DEBUG_BACKTRACE_IGNORE_ARGS);
        $trace = ob_get_contents();
        ob_end_clean();
        $trace = self::removeAllReferences($trace);
        $trace = explode(PHP_EOL, $trace);
        $trace = array_reverse($trace);
        array_shift($trace);
        $trace = array_reverse($trace);
        return $trace;
    }

    private static function highlightDocument(string $path, int $iLine): string
    {
        require '..' . DS . 'libs' . DS . 'highlight' . DS . 'Highlight.php';
        #\South\Loader\Autoload::getInstance()->lib(\South\Loader\Autoload::getInstance()::CORE, 'Highlight\\', \South\Util\Tools::normalizePath('highlight'));
        return \Highlight\Highlight::render($path, 5, $iLine, true);

    }
}
