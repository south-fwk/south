<?php
namespace South\App\Error;

abstract class Api
{

    public static function register()
    {
        register_shutdown_function(function () {
            $error = error_get_last();
            if (count($error) > 0) {self::shutdown($error);}
        });
        set_exception_handler(function ($exception) {
            self::exceptionHandler($exception);
        });
        set_error_handler(function ($numero, $mensaje, $archivo, $linea) {
            self::errorHandler($numero, $mensaje, $archivo, $linea);
        });
    }

    private static function shutdown($error)
    {
        #\South\Util\Tools::pr($error);
        $error = new \South\Http\Api\Error();
        $exception = new \stdClass;
        $exception->scope = 'SHUTDOWN';
        $exception->message = \South\Util\Tools::removeReferencesNameSpace($error["message"]);
        $exception->code = null;
        $exception->type = $error['type'];
        $exception->line = $error["line"];
        $exception->file = \South\Util\Tools::removeReferencesPath($error["file"]);
        $error->setException($exception);

        self::show($error);
    }

    private static function exceptionHandler($_exception)
    {
        $error = new \South\Http\Api\Error();
        $exception = new \stdClass;
        $exception->scope = 'EXCEPTION_HANDLER';
        $exception->message = \South\Util\Tools::removeReferencesNameSpace($_exception->getMessage());
        $exception->code = $_exception->getCode();
        $exception->line = $_exception->getLine();
        $exception->file = \South\Util\Tools::removeReferencesPath($_exception->getFile());
        $error->setException($exception);

        self::show($error);
    }

    private static function errorHandler($code, $message, $file, $line)
    {
        $error = new \South\Http\Api\Error();
        $exception = new \stdClass;
        $exception->scope = 'ERROR_HANDLER';
        $exception->message = \South\Util\Tools::removeReferencesNameSpace($message);
        $exception->code = $code;
        $exception->line = $line;
        $exception->file = \South\Util\Tools::removeReferencesPath($file);
        $error->setException($exception);

        self::show($error);
    }

    private static function headers(int $statusCode)
    {
        \South\Http\StatusCode::headerStatusCode($statusCode);
        header('Content-Type: application/json; charset=utf-8');
    }

    public static function exception($_exception, $code = '')
    {
        $error = new \South\Http\Api\Error();
        $exception = new \stdClass;
        $exception->scope = empty($code) ? 'EXCEPTION_UNCRONTROLLED' : $code;
        $exception->message = \South\Util\Tools::removeReferencesNameSpace($_exception->getMessage());
        $exception->code = $_exception->getCode();
        $exception->line = $_exception->getLine();
        $exception->file = \South\Util\Tools::removeReferencesPath($_exception->getFile());
        $error->setException($exception);

        self::show($error);
    }

    private static function show(\South\Http\Api\Error $error)
    {
        $wrapper = new \South\Http\Api\Wrapper();
        $error->setCode(\South\Http\StatusCode::INTERNAL_ERROR_SERVER);
        $error->setMessage(\South\Http\StatusCode::getMessage(\South\Http\StatusCode::INTERNAL_ERROR_SERVER));
        $wrapper->setError($error);
        $wrapper->setStatusCode(\South\Http\StatusCode::INTERNAL_ERROR_SERVER);
        self::headers($wrapper->getStatusCode());
        $wrapper->getServerInformation()->snapResponseTime();
        print(\json_encode($wrapper));
        exit;
    }

}
