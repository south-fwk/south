<?php
namespace South\App;

interface IApplication
{
    public function run();
}
