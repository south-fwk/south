<?php
namespace South\App;

use \South\Http\StatusCode;

class Api extends Application implements IApplication
{

    public function run()
    {

        Error\Api::register();

        if (!($this->controller instanceof \South\Controller\Api)) {
            throw new \South\Exception\ApplicationException("Application type dont't allow parent type for [{$this->controllerName}]");
        }

        try {
            $wrapper = call_user_func_array(array($this->controller, $this->route->getAction()), $this->route->getParameters()->get());
        } catch (\Exception $e) {
            throw new \South\Exception\RunningControllerException("An error occurred while running controller [{$this->controllerName}]", 0, $e);
        }

        if (!($wrapper instanceof \South\Http\Api\Wrapper)) {
            throw new \South\Exception\ApplicationException("Result type dont't allow for [{$this->controllerName}]");
        }

        $this->headers($wrapper->getStatusCode());
        $wrapper->getServerInformation()->snapResponseTime();
        print(\json_encode($wrapper));

        exit;
    }

    private function headers(int $statusCode)
    {
        \South\Http\StatusCode::headerStatusCode($statusCode);
        header('Content-Type: application/json; charset=utf-8');
    }

}
