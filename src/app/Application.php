<?php
namespace South\App;

use \South\Configuration;
use \South\Http\Router;

class Application
{
    protected $route;
    protected $controller;
    protected $controllerName;
    public function __construct()
    {
        $this->route = Router::getInstance()->getCurrentRoute();
        $applicationNamespace = Configuration\Provider::getInstance()->getApplicationNameSpace();
        $controllerName = "\\South\\Application\\Controllers\\{$this->route->getController()}";
        $this->controllerName = $controllerName;
        $this->controller = new $controllerName();
        $this->controller->initialize($this->route);
    }

    public function run()
    {
        print __CLASS__;
        exit;
    }
}
