<?php
namespace South\Configuration;

class Property
{

    private $path;
    private $name;

    /**
     * @var mixed $value
     */
    private $value;

    public function __construct()
    {
        $this->path = '';
        $this->name = '';
        $this->value = null;
    }

    public function setPath(string $path): Property
    {
        $this->path = $path;
        return $this;
    }

    public function setName(string $name): Property
    {
        $this->name = $name;
        return $this;
    }

    public function setValue($value): Property
    {
        $this->value = $value;
        return $this;
    }

    public function getPath(): string
    {
        return $this->path;
    }

    public function getName(): string
    {
        return $this->name;
    }

    public function getValue()
    {
        return $this->value;
    }
}
