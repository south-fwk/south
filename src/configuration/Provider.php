<?php
namespace South\Configuration;

final class Provider
{

    private $configurations;
    private $path;
    private $pathEnviroment;
    private $applicationNamespace;
    private $applicationPath;
    private $corePath;

    /**
     * @var \South\Configuration\Assets\Assets
     */
    private $assets;

    /**
     * @var \South\Configuration\Provider
     */
    private static $instance = null;

    /**
     *
     * @return \South\Configuration\Provider
     *
     */
    public static function getInstance(): Provider
    {
        if (self::$instance == null) {
            $args = func_get_args();
            $corePath = \sizeof($args) > 0 ? $args[0] : '';
            self::$instance = new self($corePath);
        }
        return self::$instance;
    }

    public function __construct(string $corePath = '')
    {
        $this->configurations = null;
        $this->path = null;
        $this->pathEnviroment = null;
        $this->corePath = $corePath;
    }

    public function load(string $dir, string $applicationNamespace): Provider
    {
        $this->applicationNamespace = $applicationNamespace;
        $this->applicationPath = $dir;
        $this->path = file_get_contents($dir . DS . 'configurations.path');
        $this->configurations = \json_decode(file_get_contents($this->path . 'configurations.json'));
        $this->pathEnviroment = $this->path . $this->configurations->enviroment . DS;

        if (property_exists($this->configurations, 'include')) {

            $oInclude = new \stdClass;
            foreach ($this->configurations->include as $include) {
                $file = $this->pathEnviroment . $include . '.json';
                if (file_exists($file)) {
                    $oInclude->{$include} = \json_decode(file_get_contents($file));
                } else {
                    throw new \South\Exception\ConfigurationException(" Don't exists file of configurations [$file]");
                }
            }
            $this->configurations->include = $oInclude;
        }

        return $this;
    }

    public function get(string $path, string $parent = ''): Property
    {
        $exists = false;
        $parts = explode('.', $path);

        if (property_exists($this->configurations, 'default_include')
            && !empty($this->configurations->default_include)
        ) {
            $parent = $this->configurations->default_include;
        }

        $parent = strtolower($parent);

        $obj = null;
        if (!empty($parent)) {
            if ($parent != 'common' && property_exists($this->configurations->include, $parent)) {
                $obj = $this->configurations->include->{$parent};
            }
        }

        $searchNode = function (&$parts, &$obj, &$exists) {
            foreach ($parts as $k => $part) {
                if (property_exists($obj, $part)) {
                    $exists = true;
                    $obj = $obj->{$part};
                } else {
                    $exists = false;
                    $obj = null;
                    break;
                }
            }
        };

        if ($obj != null) {
            $searchNode($parts, $obj, $exists);
        }

        if (!$exists && $obj == null && property_exists($this->configurations->include, 'common')) {
            $obj = $this->configurations->include->common;
            $searchNode($parts, $obj, $exists);
        }

        $property = new Property();
        $property->setName(end($parts));
        $property->setPath($path);
        $property->setValue($obj);

        if (!$exists) {
            throw new \South\Exception\ConfigurationException("Don't exists configuration key [$path]");
        }

        return $property;
    }

    public function setConfigurationAssets(\stdClass $obj)
    {
        $this->assets = new Assets\Assets();
        $this->assets->setPath($obj->path);

        $this->assets->setJavaScript(new Assets\JavaScript());
        $this->assets->getJavaScript()->setPath($obj->js->path);

        $this->assets->setCss(new Assets\Css());
        $this->assets->getCss()->setPath($obj->css->path);

        $this->assets->setImages(new Assets\Images());
        $this->assets->getImages()->setPath($obj->images->path);

        #\South\Util\Tools::pr($this->assets);
    }

    public function getConfigurationAssets(): \South\Configuration\Assets\Assets
    {
        return $this->assets;
    }

    public function getAll(): \stdClass
    {
        return $this->configurations;
    }

    public function getApplicationPath(): string
    {
        return $this->applicationPath;
    }

    public function getApplicationNameSpace(): string
    {
        return $this->applicationNamespace;
    }

    public function getCorePath(): string
    {
        return $this->corePath;
    }

    public function getRelativeRoute(): string
    {
        return $this->configurations->route->relative;
    }

    public function getApplicationName(): string
    {
        return $this->configurations->app_name;
    }

    public function getApplicationType(): string
    {
        return $this->configurations->type;
    }

    public function setDefaultInclude(string $default): Provider
    {
        if (!property_exists($this->configurations, 'default_include')
            || empty($this->configurations->default_include)
        ) {
            $this->configurations->default_include = $default;
        }
        return $this;
    }
}
