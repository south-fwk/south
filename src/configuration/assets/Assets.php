<?php
namespace South\Configuration\Assets;

class Assets extends Base
{

    /**
     * @var \South\Configuration\Assets\JavaScript
     */
    private $javaScript;

    /**
     * @var \South\Configuration\Assets\Css
     */
    private $css;

    /**
     * @var \South\Configuration\Assets\Images
     */
    private $images;

    public function setJavaScript(JavaScript $javaScript): Assets
    {
        $this->javaScript = $javaScript;
        return $this;
    }

    public function getJavaScript(): JavaScript
    {
        return $this->javaScript;
    }

    public function setCss(Css $css): Assets
    {
        $this->css = $css;
        return $this;
    }

    public function getCss(): Css
    {
        return $this->css;
    }

    public function setImages(Images $images): Assets
    {
        $this->images = $images;
        return $this;
    }

    public function getImages(): Images
    {
        return $this->images;
    }
}
