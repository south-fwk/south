<?php
namespace South\Configuration\Assets;

class Base
{

    protected $path;

    public function __construct()
    {
        $this->path = '';
    }

    public function setPath(string $path): Base
    {
        $this->path = $path;
        return $this;
    }

    public function getPath(): string
    {
        return $this->path;
    }

}
