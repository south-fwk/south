<?php
namespace Highlight;

class Highlight
{

    private static $cast = '038C8C';
    private static $null = '0000FF';
    private static $bool = 'D8C300';
    private static $self = '1D6F0C';
    private static $quote = 'FF0000';
    private static $number = 'A4AC21';
    private static $comment = 'FEA500';
    private static $tag_open = 'F00000';
    private static $keywords = '008000';
    private static $function = '0000FF';
    private static $variable = '2071ED';
    private static $constant = '8C4D03';
    private static $tag_close = 'F00000';
    private static $operators = '0000FF';
    private static $parenthesis = '038C8C';
    private static $php_function = '6367A7';
    private static $curly_braces = '7F5217';
    private static $square_bracket = 'F46164';
    private static $custom_function = 'A611AA';
    private static $multi_line_comment = 'FEA500';



    private static $self_ptrn = '/(?<!\$|\w)self/';
    private static $cast_ptrn = '/(\(\s*(int|string|float|array|object|unset|binary|bool)\s*\))/';
    private static $bool_ptrn = '/\b(?<!\$)true|false/i';
    private static $null_ptrn = '/\b(?<!\$)(null)\b/';
    private static $quote_ptrn = '/(?<!(style|class)=)"(?!\sclass=|>).*?(?<!(style|class)=)"(?!\sclass=|>)|\'.*?\'/';
    private static $number_ptrn = '/\b(\d+)\b/';
    private static $comment_ptrn = '/(?<!(http(s):))\/\/.*|(?<!color:)#.*/';
    private static $variable_ptrn = '/\$(\$*)[a-zA-Z_]+[a-zA-Z0-9_]*/';
    private static $function_ptrn = '/(?<=\s)(function)(?=\s)/';
    private static $constant_ptrn = '/\b(?<!\#)([A-Z_]+)(?!<\/\w+>\()\b/';
    private static $keywords_ptrn = '/(?<!\$|\w)((a(bstract|nd|rray(?!\s*\))|s))|
        (c(a(llable|se|tch)|l(ass(?!=)|one)|on(st|tinue)))|
        (d(e(clare|fault)|ie|o))|
        (e(cho|lse(if)?|mpty|nd(declare|for(each)?|if|switch|while)|val|x(it|tends)))|
        (f(inal|or(each)?))|
        (g(lobal|oto))|
        (i(f|mplements|n(clude(_once)?|st(anceof|eadof)|terface)|sset))|
        (n(amespace|ew))|
        (p(r(i(nt|vate)|otected)|ublic))|
        (re(quire(_once)?|turn))|
        (s(tatic|witch))|
        (t(hrow|r(ait|y)))|
        (u(nset(?!\s*\))|se))|
        (__halt_compiler|break|list|(x)?or|var|while))\b/';
    private static $tag_close_ptrn = '/<.*>(\?)<.*><.*>(\&gt;)<.*>/';
    private static $operators_ptrn = '/(\=|\.|\!|\+|\%|\-|(?<!https|http)\:|\@|\||\?|&gt;|&lt;|&amp;)/';
    private static $parenthesis_ptrn = '/\(|\)/';
    private static $curly_braces_ptrn = '/\{|\}/';
    private static $square_bracket_ptrn = '/\[|\]/';
    private static $multi_line_comment_ptrn = '/\/\*(.*?)\*\//s';



    /**
     * check if code is a file or a string then renders accordingly
     *
     * @param string $code
     * @param bool $is_file
     * @return string
     */
    public static function render($code, $rangeLines = 0, $iLine = 0, $is_file = false)
	{
        if ($is_file) {
            $code = file_get_contents($code);
        }
        return self::format($code, $rangeLines, $iLine);
	}


    /**
     * updates attributes of class property
     *
     * @param array|string $prop_name
     * @param array|string $values
     * @return void
     */
    public static function set($prop_name = array(), $values = array())
    {
        if ( is_array($prop_name)) {
            if ( is_string($values)) {
                $values = array($values);
            }
            foreach ( $prop_name as $key => $properties)
            {
                if (isset($values[$key])) {
                    self::$$properties = $values[$key];
                }
            }
        }
        else{
            self::$$prop_name = $values;
        }

    }


    /**
     * adds code to a span tag
     *
     * @param string $color
     * @param string $class
     * @param string $content
     * @return string
     */
    private static function span($color, $class, $content = '$0')
    {
        $span = sprintf('<span style="color:#%s" class="%s">%s</span>', $color, $class, $content);
        return $span;
    }


    /**
     * php preg replace function
     *
     * @param string $pattern
     * @param string $replacement
     * @param string $subject
     * @return mixed
     */
    private static function PR($pattern, $replacement, $subject, $line = 0, $iLine = 0)
    {
        //$pattern = trim(preg_replace('/\s\s+/', '', $pattern));
        $nLine = '';
        $highlight = '';
        if($line > 0){
            $nLine = "<div class=\"s-number\">{$line}</div>";            
            if($line == $iLine){
                $highlight = 's-highlight';
            }
        }

        return ('
            <div class="s-line '.$highlight.'">'.
                $nLine.
                '<div class="s-code">'.
                    str_replace("\t",'<span class="space">&nbsp;&nbsp;&nbsp;&nbsp;</span>',preg_replace($pattern, $replacement, $subject)).
                    '<div style="clear:both"></div>
                </div>
            </div>
        ');
    }


    /**
     * check and highlight user defined  or php pre defined function
     *
     * @param string $code
     * @return string
     */
    private static function isFunction($code)
    {
        return preg_replace_callback('/(\w+)(?=\s\(|\()/', function ($arg)
        {
            $func = $arg[1];
            if (function_exists($func)) {
                return self::span(self::$php_function, 'php_function', $func);
            }
            else {
                return self::span(self::$custom_function, 'custom_function', $func);
            }
        }, $code);
    }


    /**
     * creates a script that strips out all tag inside the comment or quote tag
     *
     * @return string
     */
    private static function stripCodes()
    {
        return '<script> var p = document.getElementsByClassName("strip");
            for(var i = 0; i < p.length; i++) {
                console.log(p[i].innerHTML);
                p[i].innerHTML = p[i].innerHTML.replace(/<(?!br\s*\/?)[^>]+>/g, "");
            } </script>';
    }

    /**
     * highlights code
     *
     * @param $code
     * @return string
     */
    private static function format($code, $rangeLines = 0, $iLine = 0)
    {
		$code = str_replace(
			array('<?php', '<?=', '?>'),
			array('PHP_LONG_TAG_OPEN', 'PHP_SHORT_TAG_OPEN', 'PHP_CLOSE_TAG'), 
			$code
		);
		
        $code = htmlspecialchars($code, ENT_NOQUOTES);
        $new_code = null;
        $contents = preg_split('/\n/', $code);

        $init = $iLine - $rangeLines < 0 ? 1 : $iLine - $rangeLines;
        $init = $init - 1 < 0 ? 1 : $init;
        $end = $iLine + $rangeLines > \sizeof($contents) ? \sizeof($contents) : $iLine + $rangeLines;
        
        for($i = $init - 1; $i < $end; $i++){
            $lines = $contents[$i];

			$pattern = array(
				self::$operators_ptrn,
				self::$number_ptrn,
				trim(preg_replace('/\s\s+/', '', self::$keywords_ptrn)),
				self::$function_ptrn,
				self::$variable_ptrn,
				self::$cast_ptrn
			);
			
			$replacement = array(
				self::span(self::$operators, 'operators'),
				self::span(self::$number, 'number'),
				self::span(self::$keywords, 'keyword'),
				self::span(self::$function, 'function', '$1'),
				self::span(self::$variable, 'variable'),
				self::span(self::$cast, 'cast')
			);
			
			$new_line = self::PR($pattern, $replacement, $lines);
			
			$new_line = self::isFunction($new_line);
			
			$pattern = array(
				self::$constant_ptrn,
				self::$parenthesis_ptrn,
				self::$curly_braces_ptrn,
				self::$square_bracket_ptrn,
				self::$null_ptrn,
				self::$self_ptrn,
				self::$bool_ptrn,
				self::$comment_ptrn,
				'/PHP_LONG_TAG_OPEN/',
				'/PHP_SHORT_TAG_OPEN/',
				'/PHP_CLOSE_TAG/'
			);
			
			$replacement = array(
				self::span(self::$constant, 'constant'),
				self::span(self::$parenthesis, 'parenthesis'),
				self::span(self::$curly_braces, 'curly_braces'),
				self::span(self::$square_bracket, 'square_bracket'),
				self::span(self::$null, 'null'),
				self::span(self::$self, 'self'),
				self::span(self::$bool, 'bool'),
				self::span(self::$comment, 'strip comment'),
				self::span(self::$tag_open, 'tag long', '&lt;?php'),
				self::span(self::$tag_open, 'tag short', '&lt;?='),
				self::span(self::$tag_close, 'tag clode', '?>')
			);
			$new_code .= self::PR($pattern, $replacement, $new_line,$i + 1, $iLine);
        }
		
        return sprintf('%s%s', $new_code, self::stripCodes());
    }

}