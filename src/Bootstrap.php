<?php
namespace South;

defined('DS') or define('DS', DIRECTORY_SEPARATOR);
require_once 'loader' . DS . 'Autoload.php';

final class Bootstrap
{

    private $applicationDir;
    private static $applicationNamespace;

    /**
     * @var \South\Bootstrap
     */
    private static $instance = null;

    /**
     *
     * @return \South\Bootstrap
     *
     */
    public static function getInstance(): Bootstrap
    {
        if (self::$instance == null) {
            $args = func_get_args();
            self::$applicationNamespace = \sizeof($args) > 0 ? $args[0] : '';
            self::$instance = new self();
        }
        return self::$instance;
    }

    public function __construct()
    {
        $autoLoad = Loader\Autoload::getInstance();
        $autoLoad->register(__NAMESPACE__, __DIR__);
        Configuration\Provider::getInstance(__DIR__);
    }

    public function run(string $applicationDir)
    {

        $configurations = Configuration\Provider::getInstance();
        $configurations->load($applicationDir, self::$applicationNamespace);

        $requestInformation = new Http\RequestInformation();
        $server = Server\Server::getInstance();
        $uri = explode('?', $server->REQUEST_URI);
        $requestInformation->setUri($uri[0]);
        $requestInformation->setMethod($server->REQUEST_METHOD);
        $requestInformation->setPort($server->SERVER_PORT);
        $protocol = explode('/', $server->SERVER_PROTOCOL);
        $requestInformation->setProtocol($protocol[0]);
        $requestInformation->setHost($server->HTTP_HOST);
        $requestInformation->setDataGet($_GET);
        Http\Request::getInstance()->setInformation($requestInformation)->init();

        require_once $applicationDir . DS . 'routes' . DS . 'main.php';

        $dispatcher = Dispatcher::getInstance();
        $dispatcher->go();
    }
}
